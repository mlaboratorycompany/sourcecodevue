# project-vue

Сборка учитывает использование сервисов [FaviconGenerator](https://realfavicongenerator.net) и [font2web](http://www.font2web.com), для этого созданы директории в `src/assets`

## Установка проекта
```
$ yarn install
  OR
$ npm install
```

### Запуск сервера `development`
```
$ yarn start
  OR
$ npm run start
```

### Сборка `production`

Перед сборкой убедитесь что заполнен `kraken.config.js`, данные можно получить на [kraken.io](https://kraken.io)

```
$ yarn build
  OR
$ npm run build
```

### Lints and fixes files
```
$ yarn lint
  OR
$ npm run lint
```

### Run your unit tests
```
$ yarn test:unit
  OR
$ npm run test:unit
```

