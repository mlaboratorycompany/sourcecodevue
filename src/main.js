/* eslint  no-multi-assign: off */

import Vue from 'vue';
import './plugins/vuetify';
import axios from 'axios';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.http = Vue.prototype.$http = axios;
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
