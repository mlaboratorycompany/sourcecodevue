/* eslint  no-shadow: off */

import axios from 'axios';

const state = {
  test: 'test',
};

const getters = {
  getTest(state) {
    return state.test;
  },
};

const mutations = {
  CHANGE_TEST(state, test) {
    state.test = test;
  },

};

const actions = {
  changeTest({ commit }, test) {
    axios.get('//labstory.online/direct/test.php')
      .then((res) => {
        console.log('res', res);
      })
      .catch(err => console.log('err', err));

    commit('CHANGE_TEST', test);
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
