const { secret, key } = require('./kraken.config');

module.exports = {
  chainWebpack: (config) => {
    const imagesRule = config.module.rule('images');
    const svgRule = config.module.rule('svg');

    imagesRule.uses.clear();

    imagesRule
      .use('file-loader')
      .loader('file-loader')
      .tap(() => ({
        name: '[name].[ext]',
        outputPath: (url, resourcePath) => {
          if (/fonts/.test(resourcePath)) {
            return `fonts/${url}`;
          } if (/icons/.test(resourcePath)) {
            return `${url}`;
          }
          return `img/${url}`;
        },
      }));

    svgRule
      .use('file-loader')
      .loader('file-loader')
      .tap(() => ({
        name: '[name].[ext]',
        outputPath: (url, resourcePath) => {
          if (/fonts/.test(resourcePath)) {
            return `fonts/${url}`;
          } if (/icons/.test(resourcePath)) {
            return `${url}`;
          }
          return `img/${url}`;
        },
      }));

    config.module
      .rule('xml')
      .test(/\.xml$/)
      .use('file-loader')
      .loader('file-loader')
      .tap(() => ({
        name: '[name].[ext]',
        outputPath: (url, resourcePath) => {
          if (/fonts/.test(resourcePath)) {
            return `fonts/${url}`;
          } if (/icons/.test(resourcePath)) {
            return `${url}`;
          }
          return `files/${url}`;
        },
      }));

    config.module
      .rule('webmanifest')
      .test(/\.webmanifest$/)
      .use('file-loader')
      .loader('file-loader')
      .tap(() => ({
        name: '[name].[ext]',
      }));

    config.module
      .rule('ico')
      .test(/\.ico$/)
      .use('file-loader')
      .loader('file-loader')
      .tap(() => ({
        name: '[name].[ext]',
      }));

    if (process.env.NODE_ENV === 'production') {
      imagesRule
        .use('kraken-loader')
        .loader('kraken-loader')
        .tap(() => ({
          secret,
          key,
        }));
      svgRule
        .use('kraken-loader')
        .loader('kraken-loader')
        .tap(() => ({
          secret,
          key,
        }));
    }
  },

  lintOnSave: false,
};
