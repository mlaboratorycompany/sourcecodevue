module.exports = {
  root: true,

  env: {
    node: true,
  },

  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],

  rules: {
    'no-console': 'off',
    'no-debugger': 'off',
    'no-param-reassign': 'off',
    'import/no-unresolved': 'off',
    'linebreak-style': [
      'error',
      'windows',
    ],
  },

  parserOptions: {
    parser: 'babel-eslint',
  },
};
